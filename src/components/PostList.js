import axios from 'axios';
import React, {useState, useEffect} from 'react';
import{Link} from 'react-router-dom';
import { BsPencil } from "react-icons/bs";
import { BsTrash } from "react-icons/bs";


function PostList() {
    const [posts, setPosts] = useState([]);

    const getPosts = async () => {
        const postsRes = await axios.get("http://localhost:5000/");
        const data = postsRes.data;
        setPosts(data);
    }

    const onDelete = async (id) => {
        await axios.delete(`http://localhost:5000/${id}`);
        getPosts();
    }

    useEffect(()=> {
        getPosts();
    }, [])

    return (
        <div className="container">
            <div className="mt-3">
                <h3 className="mb-4">Recent posts</h3>
                <div className="row">
                {
                    posts.map(post => (
                        <div className="col-md-4 mb-3">
                            <div className="card">
                                <img class="card-img-top" src={post.image} alt={post.title} />
                                <div className="card-body">
                                    <h5 className="card-title">{post.title}</h5>
                                    <h6 class="card-subtitle mb-2 text-muted">{post.category.name}</h6>
                                    <div className="row">
                                        <Link to={`/${post.id}`} className="card-link col-8 col-md-5 col-lg-7">
                                            See more...
                                        </Link>
                                        <Link to={`/edit/${post.id}`} className="card-link">
                                            <button type="button" className="btn btn-outline-warning">
                                                <BsPencil />
                                            </button>
                                        </Link>
                                        <Link to="#">
                                            <button type="button" className="btn btn-outline-dark ml-2" onClick={()=>onDelete(post.id)}>
                                                <BsTrash />
                                            </button>
                                        </Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                    ))
                }
                </div>
                
                
            </div>
        </div>
    )
}

export default PostList
