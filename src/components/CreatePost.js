import React from 'react';
import axios from 'axios';
import { useHistory } from 'react-router-dom';
import PostForm from './PostForm';



function CreatePost() {

    const history = useHistory();
    
    const onSubmit = async (data) => {
        await axios.post("http://localhost:5000/", data);
        history.push('/')
    }

    return (
        <div className="container">
            <div className="mt-3">
                <h3>Create a new post</h3>
                <PostForm onSubmit={onSubmit} />
            </div> 
        </div>
    )
}

export default CreatePost
