import React, {useState, useEffect} from 'react';
import axios from 'axios';
import {useRouteMatch} from 'react-router-dom';
import Moment from 'react-moment';

function Post() {
    const match = useRouteMatch();
    const [post, setPost] = useState({});

    useEffect(()=>{
        const getPost = async () => {
            const post = await axios.get(`http://localhost:5000/${match.params.id}`);
            setPost(post.data)
        }
        getPost();
    }, []);

    return (
        <div className="container">
            <h3 className="mt-3">{post.title}</h3>
            <h6 className="text-muted">
            <Moment format="D MMM YYYY HH:mm">
            {post.createdAt}
            </Moment>
            </h6>
            <div className="row">
                <p className="mt-3 col-lg-6 text-justify">{post.body}</p>
                <img class="card-img-top mt-3 col-lg-6" src={post.image} alt={post.title} />
            </div>
        </div>
    )
}

export default Post
