import React, {useState, useEffect} from 'react';
import axios from 'axios';
import { useRouteMatch, useHistory } from 'react-router-dom';
import PostForm from './PostForm';

function EditPost() {
    const match = useRouteMatch();
    const [post, setPost] = useState();

    const history = useHistory();

    useEffect(()=>{
        const getPost = async () => {
            const post = await axios.get(`http://localhost:5000/${match.params.id}`);
            setPost(post.data)
        }
        getPost()
    }, []);

    const onSubmit = async (data) => {
        await axios.patch(`http://localhost:5000/${match.params.id}`, data);
        history.push('/')
    }

    return post ? (
        <div className="container">
            <div className="mt-3">
                <h3>Edit a post</h3>
                <PostForm post={post} onSubmit={onSubmit} />
            </div>
        </div>
    ) : (
        <div>Loading...</div>
    );
}

export default EditPost
