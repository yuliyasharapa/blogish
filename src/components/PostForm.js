import React from 'react';
import {useForm} from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { PostSchema } from '../validations/PostValidation'; 

function PostForm({post, onSubmit}) {
    const {register, handleSubmit, errors} = useForm({
        resolver: yupResolver(PostSchema),
        defaultValues: {
            title: post ? post.title : "",
            body: post ? post.body : "",
            image: post ? post.image : "",
        }
    });

    const submitHandler = handleSubmit((data)=> {
        onSubmit(data);
    })


    return (
        <form onSubmit={submitHandler}>
            <div className="form-group">
                <label htmlFor="title">Title: </label>
                <input ref={register} className="form-control" type="text" name="title" id="title" />
                {errors.title && <small class="form-text text-muted">{errors.title.message}</small>}
            </div>
            <div className="form-group">
                <label htmlFor="body">Text: </label>
                <textarea ref={register} className="form-control" rows="5" name="body" id="body" />
                {errors.body && <small class="form-text text-muted">{errors.body.message}</small>}
            </div>
            <div className="row">
            <div className="form-group col-8">
                <label htmlFor="image">Image URL: </label>
                <input ref={register} className="form-control" type="text" name="image" id="image" />
                {errors.image && <small class="form-text text-muted">{errors.image.message}</small>}
            </div>
            <div className="form-group col-4">
                <label htmlFor="category">Category: </label>
                <select ref={register} className="form-control" name="categoryId" id="category">
                    <option value="1">Travel</option>
                    <option value="2">Food</option>
                    <option value="3">Psychology</option>
                    <option value="4">News</option>
                </select>
            </div>
            </div>
            
            <div className="form-group">
                <button type="submit" className="btn btn-primary">
                    Save
                </button>
            </div>
        </form>
    )
}

export default PostForm
