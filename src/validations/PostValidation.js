import * as yup from "yup";

export const PostSchema = yup.object().shape({
    title: yup
    .string()
    .required("Required"),
    body: yup
    .string()
    .required("Required"),
    image: yup
    .string()
    .url("Must be a URL")
    .required("Required"),
})