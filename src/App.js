import {Route, Switch, Link} from 'react-router-dom';
import CreatePost from './components/CreatePost';
import EditPost from './components/EditPost';
import Post from './components/Post';
import PostList from './components/PostList';

function App() {
  return (
    <div className="App">
      <nav className="navbar bg-light navbar-expand-lg navbar-light">
        <ul className="navbar-nav mr-auto">
          <li className="navbar-item">
            <Link to="/" className="nav-link">Posts</Link>
          </li>
          <li className="navbar-item">
            <Link to="/create" className="nav-link">New Post</Link>
          </li>
        </ul>
      </nav>
      <Switch>
        <Route exact path="/" component={PostList} />
        <Route path="/create" component={CreatePost} />
        <Route path="/edit/:id" component={EditPost} />
        <Route path="/:id" component={Post} />
      </Switch>
    </div>
  );
}

export default App;
